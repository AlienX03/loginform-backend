using System.Security.Claims;

using System;
namespace Timesheet
{
    using Nancy.Authentication.Forms;
    using Nancy;
    using System.Security.Principal;

    public class UserMapper : IUserMapper
    {

        public ClaimsPrincipal GetUserFromIdentifier(Guid identifier, NancyContext context)
        {
            var record = UserRepo.findUserByGuid(identifier, IndexModule.dbModule.connection);
            return record == null ? null : new ClaimsPrincipal(new GenericIdentity(record.ToString()));
        }



    }

}