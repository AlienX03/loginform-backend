namespace Timesheet {
    using Nancy;
    using System;
    
    public class Security {
        public static bool isAuthorised(NancyContext context){

            string cookie;
            context.Request.Cookies.TryGetValue("_ncfa",out cookie);
            Guid guid = FormAuthBootstrapper.getGuid(cookie);
            return checkDb(guid);
        }


        private static bool checkDb(Guid guid){

            UserDto user = UserRepo.findUserByGuid(guid,IndexModule.dbModule.connection);
            Console.WriteLine(user.ToString());
            return (user.type=="admin");
            
        }
    }
}