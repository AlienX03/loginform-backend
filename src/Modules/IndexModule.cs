using System.Linq;
namespace Timesheet
{
    using System;
    using Nancy;
    using Nancy.ModelBinding;
    using Nancy.Authentication.Forms;
    using Nancy.Security;
    public class IndexModule : NancyModule
    {
        public static DbModule dbModule = new DbModule();
        public IndexModule()
        {

            Get("/login", _ =>
            {
                return "Login please";
            });

            Post("/register", (parameter) =>
            {

                var userObject = this.Bind<UserDto>();
                Guid guid = Guid.NewGuid();
                userObject.Id = guid;
                UserRepo.postUser(userObject, dbModule.connection);
                return userObject.ToString();
            });

            Post("/login", (parameter) =>
             {
                 var userObject = this.Bind<UserDto>();
                 var guid = UserRepo.findGuid(userObject, dbModule.connection);
                 DateTime? expiry = null;
                 if (this.Request.Form.RememberMe.HasValue)
                 {
                     expiry = DateTime.Now.AddDays(7);
                 }
                 
                 var authResult = this.LoginAndRedirect(guid.Value);
                 return Response.AsJson(new
                 {
                     username = userObject.username
                 }).WithCookie(authResult.Cookies.First());
                 

             });

        }

        public dynamic findUser(dynamic parameter)
        {
            return UserRepo.findUserByName(Int32.Parse(parameter), dbModule.connection).ToString();
        }
    }
}