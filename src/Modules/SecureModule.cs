using System.Net;
using System.Linq;
using System;
using System.ComponentModel;
namespace Timesheet
{
    using Nancy;
    using Nancy.Security;
    using System.Security.Principal;
    using System.Security.Claims;
    public class SecureModule : NancyModule
    {
        public SecureModule() : base("/secure")
        {
            this.RequiresAuthentication();

            Get("/check", args =>
            {
                if(Security.isAuthorised(this.Context)){
                    return "This is Secure module";
                }
                else {
                    return "this";
                }
                
            });

            Get("/", args =>
            {
                this.RequiresClaims(c => c.Type == ClaimTypes.Role && c.Value == "Admin");
                return "This is Secure module";
            });
        }
    }
}