using System.Runtime.InteropServices;
namespace Timesheet
{
    using System;
    using Npgsql;
    public class UserRepo
    {

        public static UserDto createUserDto(NpgsqlDataReader reader)
        {
            UserDto userDto = null;
            while (reader.Read())
            {
                userDto = new UserDto();
                userDto.Id = Guid.Parse(reader[0].ToString());
                userDto.username = reader[1].ToString();
                userDto.password = reader[2].ToString();
                userDto.type = reader[3].ToString();
            }
            return userDto;
        }

        public static void postUser(UserDto user, NpgsqlConnection connection)
        {
            NpgsqlCommand command = new NpgsqlCommand("INSERT INTO timesheet.user (id, username, password)	VALUES (@id, @name, @pwd);", connection);
            command.Parameters.AddWithValue("name", user.username);
            command.Parameters.AddWithValue("id", user.Id);
            command.Parameters.AddWithValue("pwd", user.password);
            command.ExecuteReader();
        }

        public static UserDto findUserByGuid(Guid id, NpgsqlConnection connection)
        {
            UserDto userDto = null;
            NpgsqlCommand command = new NpgsqlCommand("select * from timesheet.user where id=@id", connection);
            command.Parameters.AddWithValue("id", id);
            try
            {

                NpgsqlDataReader reader = command.ExecuteReader();
                command.Dispose();
                userDto = createUserDto(reader);
                reader.DisposeAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return userDto;
        }

        public static Guid? findGuid(UserDto user, NpgsqlConnection connection)
        {

            NpgsqlCommand command = new NpgsqlCommand("select * from timesheet.user where username=@username and password=@password", connection);
            command.Parameters.AddWithValue("username", user.username);
            command.Parameters.AddWithValue("password", user.password);

            UserDto userDto = null;
            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();
                command.Dispose();
                userDto = createUserDto(reader);
                reader.DisposeAsync();
                Console.WriteLine(userDto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            if(userDto==null){
                return null;
            }
            return userDto.Id;

            
        }

        public static UserDto findUserByName(int id, NpgsqlConnection connection)
        {
            UserDto userDto = null;
            NpgsqlCommand command = new NpgsqlCommand("select * from Timesheet.user where id=@id", connection);
            command.Parameters.AddWithValue("id", id);
            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();
                userDto = createUserDto(reader);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return userDto;
        }
    }
}