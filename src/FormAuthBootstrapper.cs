using System.Reflection.Metadata;

namespace Timesheet
{
    using System;
    using Nancy.Authentication.Forms;
    using Nancy.Bootstrapper;
    using System.Security.Claims;
    using System.Collections.Generic;
    using Nancy.TinyIoc;
    using Nancy;
    public class FormAuthBootstrapper : DefaultNancyBootstrapper
    {
        public static FormsAuthenticationConfiguration formsAuthConfiguration;        

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {

        }
        protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context)
        {
            base.ConfigureRequestContainer(container, context);

            container.Register<IUserMapper, UserMapper>();
        }
        protected override void RequestStartup(TinyIoCContainer requestContainer, IPipelines pipelines, NancyContext context)
        {

            formsAuthConfiguration =
                new FormsAuthenticationConfiguration()
                {
                    RedirectUrl = "/login",
                    UserMapper = requestContainer.Resolve<IUserMapper>(),
                };

            FormsAuthentication.Enable(pipelines, formsAuthConfiguration);
        }

        public static Guid getGuid(string cookie)
        {
            var guid = FormsAuthentication.DecryptAndValidateAuthenticationCookie(cookie, formsAuthConfiguration);
            Guid guid1;
            Guid.TryParse(guid, out guid1);
            return guid1;
        }

    }
}