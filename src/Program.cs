namespace Timesheet
{
    using System;
    using Nancy.Hosting.Self;
    

    class Program{
        //https://github.com/NancyFx/Nancy/issues/2989
        public static void Main(string[] args){
            Console.WriteLine("hi");
            var configuration = new HostConfiguration { RewriteLocalhost = false };
            using(var host = new NancyHost(configuration,new Uri("http://localhost:8080"))){
                host.Start();
                Console.WriteLine("NancyFX Stand alone test application.");
                Console.WriteLine("Press enter to exit the application");
                Console.ReadLine();
            }
            
        }
    }
}