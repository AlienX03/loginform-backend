# c# Nancy Backend project

A small Backend project which is developed from scratch with nancy 2.0.0 (Not preferable version for development)

## Description

- Routing api's are written inside src\IndexModule.cs
- DbModule is used to maintain singelton instance of db connection
    > NpgSql for db connection
- UserRepo will communicate with user table in DB
- Authentication and base redirection logic is done inside **UserMapper** which inherits IUserMapper Interface


